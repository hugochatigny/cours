<?php



/* Fonctione de connexion à la base de données */
function dbConnect() {
    $servername = "localhost";
    $username = "root";
    $password = "";
    $dbname = "api_poisson";

    try {
        $conn = new PDO("mysql:host=$servername;dbname=$dbname", $username, $password);
        // set the PDO error mode to exception
        $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        //   echo "Connected successfully";
        return $conn;
    } catch (PDOException $e) {
        echo "Connection failed: " . $e->getMessage();
    }
}



/* Fonctione de déconnexion de la base de données */

function dbDisconnet($conn) {
    $conn = null;
}

function getAllData(){
    $sql = "SELECT * FROM poisson";

    try {
        $co = dbConnect();
        $stmt = $co->prepare($sql);
        $stmt->execute();
        $objet = [];

        while($row = $stmt-> fetch(PDO::FETCH_ASSOC)){
            $objet[] = $row;
        }
        return json_encode($objet, JSON_INVALID_UTF8_IGNORE);

        
    } catch (\PDOException $e) {
        throw $e->getMessage();
    }
}

function getAllSpeed($vitesse){
    $sql = "SELECT * FROM poisson  WHERE vitesse_max >= :vitesse";

    try {
        $co = dbConnect();
        $stmt = $co->prepare($sql);
        $stmt->bindParam(":vitesse",$vitesse);
        $stmt->execute();
        $objet = [];

        while($row = $stmt-> fetch(PDO::FETCH_ASSOC)){
            $objet[] = $row;
        }
        return json_encode($objet, JSON_INVALID_UTF8_IGNORE);

        
    } catch (\PDOException $e) {
        throw $e->getMessage();
    }
}
