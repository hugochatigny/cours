<?php

require_once 'functions.php';

if (isset($_GET["api"])) {
    if (isset($_GET['vitesse']) && $_GET['vitesse']) {
        //afficher les poissons qui ont une vitesse supp à la valeur demandée 
        echo getAllSpeed($_GET['vitesse']);

    } else {
        echo getAllData();
    }
} else {
    echo "Aucune donnée à afficher";
}
