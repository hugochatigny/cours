<?php

/*
    Type de variable
*/

// integer
// int <- 10
$int = 10;

// float
$fl = 10.2;

// string 
// chaine de caratere
$chaine = "Yop";

// bool
$is_ok = 0;
$is_not_ok = true; // false

/*
    Tableau
*/

// tableau indicé

$monTab = ["Salut", "Le", "Monde", 42];

$monTab2 = array();

echo $monTab[3];
// j'ajoute une valeur à mon tableau
$monTab[] = "Yatta";

var_dump($monTab);

// tableau associatif
$monTabAssoc = [
    "nom" => "Riri",
    "age" => 10
];

echo $monTabAssoc["age"];

$monTabAssoc2 = [
    "hero1" => [
        "pseudo" => "El barbarian",
        "str" => 20,
        "char" => 2
    ],
    "hero2" => [
        "pseudo" => "Gandalf",
        "str" => 2,
        "char" => 12
    ]
];

echo $monTabAssoc2["hero2"]["pseudo"];

/*
    Condition
*/
$is_ok_too = false;

if ($monTabAssoc["age"] == 10) {
    echo "C'est " . $monTabAssoc["nom"] . " et il a " . $monTabAssoc["age"] . " ans";
} else if ($monTabAssoc["age"] == 18) {
    echo "majeur";
} else {
    echo "inconnu";
}

// ET &&
if ($monTabAssoc["age"] == 10  && $is_ok_too) {
    // check si les 2 sont vrais
}

// OU || 
if ($monTabAssoc["age"] == 10 || $is_ok_too) {
    // check si une des deux est vrai
}

$page = "home";

switch ($page) {
    case 'home':
    case 'accueil':
        # affiche la page d'accueil en français
        # affiche la page d'accueil en anglais
        break;
    case 'contact':
        # affiche la page de contact
        break;

    default:
        # affiche la page 404
        break;
}


/*
    boucle
*/
echo "<hr>";
echo "<h2>Boucle</h2>";
$limit = 10;
$a = 0;

// ne fait le traitement que si $a est plus petit que 10
while ($a <= 10) {
    echo "while <br>";
    //echo "list " . $a;
    $a++;
}
// $a == 10
echo "<br>";


// fait le traitement au moins une fois et controle la valeur de $a
do {
    echo "do while <br>";
    //echo "list " . $a;
} while ($a <= 10);

for ($i = 0; $i < $limit; $i++) {
    echo $i;
}

echo "<hr>";
// foreach
var_dump($monTabAssoc);
foreach ($monTabAssoc as $key => $value) {
    echo "clé '" . $key . "' valeur '" . $value . "'<br>";
}
echo "<hr>";

// tp afficher le contenu de monTabAssoc2
// AFFICHER TAB Hero

// VAR 
//      MonTabAssoc2
// DEBUT
//      Tant que montabAssoc2 à hero
//          affiche hero
//          tant que hero à stat
//              affiche stat
//          fin que
//      fin tant que
// FIN

foreach ($monTabAssoc2 as $key => $hero) {
    echo $key . "<br>";
    //var_dump($hero);
    foreach ($hero as $name => $stat) {
        echo "&nbsp;&nbsp;&nbsp;&nbsp;" . $name . " <strong>" . $stat . "</strong><br>";
    }
}
