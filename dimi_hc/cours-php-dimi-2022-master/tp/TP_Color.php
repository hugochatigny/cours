<?php
/*
1/ Créer un tableau indicé avec PHP. Ce tableau devra contenir les valeurs :

red
yellow
black
pink
grey
blue

2/ ce tableau, vous devrez le parcourir et afficher dans le HTML, une liste d’élément sauf le 4eme élément du tableau

3/ le traitement précédent devra être migré dans une fonction et l’indice de l’élément à ne pas afficher, fourni en paramètre.

Nous pourrons ainsi choisir de ne pas afficher le 2eme ou le 5eme élément.

 

4/ en remplacement de la liste HTML, afficher des div avec comme couleurs de fond celle du tableau.

 

5/ mettre un formulaire de type GET, qui va nous permettre d’envoyer le numéro à ne pas afficher

*/

$monTab = ["red", "yellow", "black", "pink", "grey", "blue"];
$monTab[] = 'aqua';

if (isset($_GET['numb']) && ($_GET['numb']) != null ) {
    $numb = $_GET['numb'];


    function Tab($monTab, $numb) {
        foreach ($monTab as $key => $value) {
            if ($key != $numb){
                echo "<div style='background-color:$value'>" . $value . "</div>";
                
    
            }
        }
    }
    Tab($monTab,$numb);
} else {
    $numb = "NULL";
}

?>

<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="preconnect" href="https://fonts.googleapis.com">
<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
<link href="https://fonts.googleapis.com/css2?family=Roboto:wght@500&display=swap" rel="stylesheet">
    <title>TP | Couleur</title>
</head>
<style>
    div{
        color: purple;
        margin-left: auto;
        font-size: 62px;
        margin-right: auto;
        text-align: center;
        width: 300px;
        box-shadow: 10px 10px 20px purple;
    }
    form{
        text-align: center;
        margin-top: 50px;
    }
    label{
        font-size: 32px;
        font-family: 'Roboto', sans-serif;
        margin-bottom: 15px;
        display: block;
        margin-right: auto;
        margin-left: auto;
    }
    input[type="text"]{
        cursor: pointer;
        padding-left: 10px;;
        width: 200px;
        height: 30px;
        border: none;
        border-radius: 30px 30px  30px  30px;
        box-shadow: 0px 0px 20px black;
    }
    input[type="text"]:focus-visible{
       outline: white;
    }
    input[type="submit"]{
        width: 150px;
        height: 40px;
        background-color: #EC816A;
        border: none;
        border-radius: 50px 50px 50px 50px;
        margin-top: 15px;
        display: block;
        margin-right: auto;
        margin-left: auto;
        transition: .2s ease-in-out;

    }
    input[type="submit"]:hover{
        cursor: pointer;
        transform: scale(1.1);
        transition: .2s ease-in-out;
    }
</style>

<body>
<form action="#" method="get">
        <label for="numb">Nombre à pas afficher</label>
        <input type="text" name="numb" id="numb">
        <input type="submit" name="SEND">
    </form>

</body>

</html>