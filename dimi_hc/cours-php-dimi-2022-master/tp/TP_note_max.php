<?php


/*
exo 1 :

Utiliser une fonction native de PHP qui va me permettre d’afficher 
la note la plus haute (afficher à l’écran “Meilleur note HTML avec 15”) 
Bien penser/trouver un moyen d’afficher la matière de manière dynamique.

*/
$notes =    [

    'français' => 10,

    'html' => 15,

    'javascript' => 9,

    'php' => 12

];
echo max($notes);
echo ' ';
echo array_search(max($notes), $notes);

echo '<hr>';

/*
exo 2:

Sans effacer l’ancien code (dans le même fichier donc), créer une fonction qui va vous permettre de récupérer la note maximum manuellement (boucle, comparaison, tableau, etc.).

*/
$meilleurNote = getNoteMax($notes);

function getNoteMax($e) {

    $meilleurNote = [
        "matiere" => "",
        "note" => 0,
    ];

    foreach ($e as $matiere => $note) {
        if ($note > $meilleurNote['note']) {
            $meilleurNote['note'] = $note;
            $meilleurNote['matiere'] = $matiere;
        }
    }
    return $meilleurNote;
}


$pireNote = getNoteMin($notes);
function getNoteMin($e) {
    $pireNote = [
        "matiere" => "",
        "note" => 0,
    ];

    foreach ($e as $matiere => $note) {
        if ($note < $pireNote['note']) {
            $pireNote['note'] = $note;
            $pireNote['matiere'] = $matiere;
        }
    }
    return $pireNote;
}

var_dump($notes);

/*
exo 3

*/

echo min($notes);
echo ' ';
echo array_search(min($notes), $notes);

echo '<hr>';

/*
exo 4

*/
$a = 0;
foreach ($notes as $key => $value) {
    $a = $a + $value;
}
$moyenne = $a / count($notes);
echo $moyenne;
echo '<hr>';

/*
exo 5
*/
echo "<style>";
if ($moyenne >= 10) {
    echo "#color{background:green}";
} else {
    echo "#color{background:red}";
}
echo "</style>";
?>
<style>
    table,
    td {
        border: 1px solid #333;
    }

    thead,
    tfoot {
        background-color: #333;
        color: #fff;
    }
</style>
<table>

    <tbody>
        <tr>
            <td>max note</td>
            <td>min note</td>
            <td>moy</td>
        </tr>
        <tr>
            <td><?php echo $meilleurNote['matiere'] ?></td>
            <td><?php echo $pireNote['matiere'] ?></td>
            <td rowspan="2" id="color"><?php echo $moyenne ?></td>
        </tr>
        <tr>
            <td><?php echo $meilleurNote['note'] ?></td>
            <td><?php echo $pireNote['note'] ?></td>

        </tr>
    </tbody>
</table>
