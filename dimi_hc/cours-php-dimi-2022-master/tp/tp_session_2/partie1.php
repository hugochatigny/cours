<?php
session_start();
/*
 * Creer une session dans ce fichier.
 * Ajouter un formulaire de contact(login, mdp) qui pointe sur le fichier partie2.php.
 * à la soumission du formulaire, vous devrez vérifier si l'utilisateur existe dans le tableau users.
 * Si l'utilisateur existe vous mettrez son nom en session, il est alors connecté. 
 * S'il n'existe pas afficher "Utilisateur inconnue"
 * ------------------------
 * Depuis la session vous afficherez le nom de celui-ci et un lien "se déconnecter" 
 * vers un autre fichier (deco.php) qui doit détruire la session 
 * et le rediriger sur la page formulaire. (voir la fonction header)
 * Le lien ne doit s'afficher que si l'utilisateur est connecté.
 * ------------------------
 * Ajouter un formulaire qui va permettre à l'utilisateur 
 * de choisir un theme (changez juste le fond d'écran et le couleur de la police genre theme sombre et theme clair)
 * 
 * ------------------------
 * /!\ l'exercice est compliqué, n'hésitez pas à découper les étapes, par exemple:
 * 1/ je verifie si le login et le mdp sont renseignés
 * 2/ je stock dans des variables ces valeurs
 * 3/ si les deux sont renseignés, je vérifie qu'elles correspondent dans le tableau users
 * 4/ etc. 
 */

?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="style1.css">

    <title>Partie 1 </title>
</head>

<body>
<?php require_once "./view/parts/menu.php" ?>

    <div class="container">
        <?php

        if (isset($_SESSION['nom']) && ($_SESSION['nom']) != null) {
            echo '<h1>'.$_SESSION['nom'].'</h1>';
        } else {
            echo '<form class="mt-5" action="./partie2.php" method="post">
            <input type="email" name="login" id="">
            <input type="password" name="mdp" id="">
            <input type="submit" value="CONNEXION">

        </form>';
        }


        ?>

        


    </div>

</body>

</html>