<?php

session_start();

$login = $_POST["login"];
$mdp = $_POST["mdp"];

$users = array(
    'utilisateur1' =>
    array(
        'nom' => 'Riri',
        'login' => 'riri@riri.com',
        'mdp' => 'S3cr3t',
        'avatar' => 'profil1.jpg'
    ),
    'utilisateur2' =>
    array(
        'nom' => 'Loulou',
        'login' => 'loulou50@gmail.com',
        'mdp' => 'L0ul0u50',
        'avatar' => 'profil2.png'
    ),
    'utilisateur3' =>
    array(
        'nom' => 'admin',
        'login' => 'admin@gmail.com',
        'mdp' => 'admin',
        'avatar' => ''
    )
);

foreach ($users as $user) {
    if (($user['login'] == $login) && ($user['mdp'] == $mdp)) {
        echo "oui";
        echo '<br>';
        $_SESSION['nom'] = $user['nom'];
        $_SESSION['avatar'] = $user['avatar'];
        header('Location: ./partie1.php');
    } else {
        $_SESSION['error'] = 'Une erreur est survenue';
        header('Location: ./partie1.php');
    }
}