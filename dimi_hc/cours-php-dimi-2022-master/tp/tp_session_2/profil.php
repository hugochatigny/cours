<?php
session_start();
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="<?php

        if (isset($_SESSION['style']) && ($_SESSION['style']) != null) {
            echo $_SESSION['style'];
        } else {
            echo "style1.css";
        }


        ?>">

    <title>Profil</title>
</head>

<body>

    <?php require_once "./view/parts/menu.php" ?>


    <div class="container mt-5">
        <div class="btn-group" role="group" aria-label="Basic radio toggle button group">
            <form action="./traitement.php" method="post">
                <input type="radio" class="btn-check" value="style1" name="style" id="btnradio1" autocomplete="off" checked="">
                <label class="btn btn-outline-primary" for="btnradio1">Radio 1</label>

                <input type="radio" class="btn-check" value="style2" name="style" id="btnradio2" autocomplete="off" checked="">
                <label class="btn btn-outline-primary" for="btnradio2">Radio 2</label>

                <input type="radio" class="btn-check" value="style3" name="style" id="btnradio3" autocomplete="off" checked="">
                <label class="btn btn-outline-primary" for="btnradio3">Radio 3</label>

                <input type="submit" value="fkzehi">
            </form>
        </div>
    </div>

</body>

</html>