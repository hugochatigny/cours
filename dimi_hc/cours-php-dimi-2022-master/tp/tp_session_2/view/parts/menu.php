<?php require_once "./view/parts/menu.php" ?>

<nav class="navbar navbar-expand-lg navbar-dark bg-primary">
        <div class="container-fluid">
            <a class="navbar-brand" href="./partie1.php">Partie 1 </a>
            <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarColor01" aria-controls="navbarColor01" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarColor01">
                <ul class="navbar-nav me-auto">
                    <li class="nav-item">
                        <a class="nav-link active" href="#">
                            <?php

                            if (isset($_SESSION['nom']) && ($_SESSION['nom']) != null) {
                                echo $_SESSION['nom'];
                            } else {
                                echo "Bonjour Homies";
                            }


                            ?>
                            <span class="visually-hidden">(current)</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="./deco.php">
                            <?php

                            if (isset($_SESSION['nom']) && ($_SESSION['nom']) != null) {
                                echo "Déconnexion";
                            }


                            ?></a>
                    </li>

                </ul>
                <div class="nav-item">

                    <?php

                    if (isset($_SESSION['avatar']) && ($_SESSION['avatar']) != null) {
                        echo '<a class="nav-link" href="./profil.php">';
                        echo '<img src="./images/' . $_SESSION['avatar'] . '" width="100px" style="border-radius:50%" alt="">';
                    } else {
                        echo '<a class="nav-link" href="./profil.php">';

                        echo '<img src="./images/default.jpg" width="100px" style="border-radius:50%" alt="">';

                    }


                    ?></a>
                </div>


            </div>
        </div>
    </nav>