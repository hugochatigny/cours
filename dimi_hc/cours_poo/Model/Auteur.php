<?php

class Auteur {
    private $nom;
    private $prenom;
    private $livres;

    function __construct($prenom="", $nom="", $livres=0)
    {
        $this->prenom = $prenom;
        $this->nom = $nom;
        $this->livres = [];
    }

    public function __toString()
    {
        return " Nom : ".$this->nom . " prenom : ". $this->prenom;
    }

    //methodes (des functions encapsulées dans l'objet)

    function addLivre($livre) {
        $this->livres[] = $livre;
    }
    function remLivre() {
        echo "retirer Livre";
    }

    /**
     * ACCESSEUR / MUTATEUR / GETTER SETTER
     */

    // les get permet de lire le propriété
    function getPrenom() {
        return $this->prenom;
    }

    // les set permet d'écrire dans la propriété
    function setPrenom($prenom) {
        if (strlen($prenom) > 0) {
            $this->prenom = strtolower($prenom);
        }
    }

    // les get permet de lire le propriété
    function getLivres() {
        return $this->livres;
    }

    // les set permet d'écrire dans la propriété
    function setLivre($livres) {
        if (strlen($livres) > 0) {
            $this->livres = strtolower($livres);
        }
    }

    // les get permet de lire le propriété
    function getNom() {
        return $this->nom;
    }

    // les set permet d'écrire dans la propriété
    function setNom($nom) {
        if (strlen($nom) > 0) {
            $this->nom = strtolower($nom);
        }
    }

}
