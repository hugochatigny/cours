<?php

class Livre {
    public $auteur;
    private $titre;
    private $page;

    function __construct($titre="", $page=0)
    {
        $this->titre = $titre;
        $this->page = $page;
    }

    //methodes (des functions encapsulées dans l'objet)

    function ouvrir() {
        echo "j'ouvre le livre";
    }
    function emprunter() {
        echo "j'emprunte le livre";
    }
    function rendre() {
        echo "je rend le livre";
    }

    /**
     * ACCESSEUR / MUTATEUR / GETTER SETTER
     */

    // les get permet de lire le propriété
    function getPage() {
        return $this->page;
    }

    // les set permet d'écrire dans la propriété
    function setPage($page) {
        if (strlen($page) > 0) {
            $this->page = strtolower($page);
        }
    }

    // les get permet de lire le propriété
    function getTitre() {
        return $this->titre;
    }

    // les set permet d'écrire dans la propriété
    function setTitre($titre) {
        if (strlen($titre) > 0) {
            $this->titre = strtolower($titre);
        }
    }
}
