<?php
require_once "./Model/Livre.php";
require_once "./Model/Auteur.php";

$livre = new Livre("Harry", 1200);
// $livre->ouvrir();

// $livre->setPage(300); // je met dans le nombre de page

// $livre->setTitre('un titre');
// $livre2->setTitre("L: change the world");
// $livre2->setPage('3000');

$livre2 = new Livre("Paul", 45);
$livre3 = new Livre("Cimetiere", 500);
$auteur->addLivre($livre3);


$auteur = new Auteur("Guillaume ", "MUSSO", 645);
$auteur->addLivre($livre);

?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Roboto&display=swap" rel="stylesheet">
    <title>Document</title>
    <style>
        h1{
            font-size: 5rem;

            text-align: center;
        }
        *{
            font-family: 'Roboto', sans-serif;
        }
        section {
            display: grid;
            grid-template-columns: 1fr 1fr;
        }

        section h2 {
            color: red;
            font-size: 4rem;
            font-weight: 600;
        }

        section p {
            color: green;
            font-size: 2rem;
            font-weight: 700;
        }
    </style>
</head>

<body>
    <h1>cours POO</h1>
    <section>
        <article>
            <h2>
                <?php
                echo $livre->getTitre()
                ?>
            </h2>
            <p>
                <?php
                echo $livre->getPage()
                ?> Page
            </p>
        </article>

        <article>
            <h2>
                <?php
                echo $livre2->getTitre()
                ?>
            </h2>
            <p>
                <?php
                echo $livre2->getPage()
                ?> Page
            </p>
        </article>

        <article>
            <h2>
                <?php
                echo $auteur->getPrenom();
                echo $auteur->getNom();
                echo "<br>";
                // echo $auteur;
                ?>
            </h2>
            <p>
                <?php
                echo $auteur->getLivres()
                ?> Livres
            </p>
        </article>
    </section>
    <section>
        
    </section>

</body>

</html>