<?php

require_once "vendor/autoload.php";

/**
 * Micro Router
 * 
 * On charge les controllers en fonction de la valeur du parametre page.
 * si le paramètre page n'est pas renseigné dans l'url,
 * on affiche la page d'accueil du HomeController par défaut.
 */

if (isset($_GET["page"]) && $_GET["page"]) {
    $controllerName = ucfirst($_GET["page"]);

    $className = "App\\Controller\\{$controllerName}Controller";

    $page = new $className();

    // Condition ternaire pour récupérer un id si nécessaire
    $id = (isset($_GET["id"]) && $_GET["id"]) ? $_GET["id"] : null;

    if (isset($_GET["action"]) && $_GET["action"]){
        $method = $_GET["action"];
        $page->$method($id);
    } else {
        $page->main($id);
    }

} else {
    $page = new App\Controller\HomeController();

    $page->main();
}
