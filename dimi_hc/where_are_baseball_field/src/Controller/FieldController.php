<?php

namespace App\Controller;

use App\Entity\Field;
use App\Repository\FieldRepository;

class FieldController {
    /**
     * Main()
     * 
     * fonction permettant d'afficher les informations d'un terrain
     */

    public function main($id) 
    {
        $fRepo = new FieldRepository();
        $field = $fRepo->getOneField($id);
        
        include 'templates/field.php';
    }
    public function formAddField()
    {
        include 'templates/form_add_field.php';
    }
    public function addField()
    {
        $f = new Field();
        $f->setName($_POST["name"]);
        $f->setTeam($_POST["team"]);
        $f->setLat($_POST["lat"]);
        $f->setLng($_POST["lng"]);
        $f->setCity($_POST["city"]);
        $f->setCountry($_POST["country"]);

        $fRepo = new FieldRepository();
        $added = $fRepo->addField($f);

        if ($added){
            $alert = "Terrain ajouté";
        } else {
            $alert = "Terrain pas ajouté";
        }

        include 'templates/form_add_field.php';
    }
}
