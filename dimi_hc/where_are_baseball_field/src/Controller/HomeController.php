<?php

namespace App\Controller;

use App\Repository\FieldRepository;

class HomeController{
    public function main()
    {
        $fRepo = new FieldRepository();
        $fields = $fRepo->getAllField();

        include 'templates/home.php';
    }
}