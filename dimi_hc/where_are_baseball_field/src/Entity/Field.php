<?php

namespace App\Entity;
class Field{

    //Propriétés ou attributs

    public $idField;
    public $name;
    public $team;
    public $description;
    public $lat;
    public $lng;
    public $street;
    public $streetBis;
    public $zip;
    public $city;
    public $country;

    // Constructeur
    public function __construct(Array $data = [])
    {
        if(!empty($data)){
            $this->setIdField($data["id_field"]);
            $this->setName($data["name"]);
            $this->setTeam($data["team"]);
            $this->setDescription($data["description"]);
            $this->setLat($data["lat"]);
            $this->setLng($data["lng"]);
            $this->setStreet($data["street"]);
            $this->setStreetBis($data["street_bis"]);
            $this->setZip($data["zip"]);
            $this->setCity($data["city"]);
            $this->setCountry($data["country"]);
            
        }
    }

    
    // Getters & Satters ou Accesseurs

    

    /**
     * Get the value of idField
     */ 
    public function getIdField()
    {
        return $this->idField;
    }

    /**
     * Set the value of idField
     *
     * @return  self
     */ 
    public function setIdField($idField)
    {
        $this->idField = $idField;

        return $this;
    }

    /**
     * Get the value of name
     */ 
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set the value of name
     *
     * @return  self
     */ 
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get the value of team
     */ 
    public function getTeam()
    {
        return $this->team;
    }

    /**
     * Set the value of team
     *
     * @return  self
     */ 
    public function setTeam($team)
    {
        $this->team = $team;

        return $this;
    }

    /**
     * Get the value of description
     */ 
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set the value of description
     *
     * @return  self
     */ 
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get the value of lat
     */ 
    public function getLat()
    {
        return $this->lat;
    }

    /**
     * Set the value of lat
     *
     * @return  self
     */ 
    public function setLat($lat)
    {
        $this->lat = $lat;

        return $this;
    }

    /**
     * Get the value of lng
     */ 
    public function getLng()
    {
        return $this->lng;
    }

    /**
     * Set the value of lng
     *
     * @return  self
     */ 
    public function setLng($lng)
    {
        $this->lng = $lng;

        return $this;
    }

    /**
     * Get the value of street
     */ 
    public function getStreet()
    {
        return $this->street;
    }

    /**
     * Set the value of street
     *
     * @return  self
     */ 
    public function setStreet($street)
    {
        $this->street = $street;

        return $this;
    }

    /**
     * Get the value of street_bis
     */ 
    public function getStreetBis()
    {
        return $this->street_bis;
    }

    /**
     * Set the value of street_bis
     *
     * @return  self
     */ 
    public function setStreetBis($street_bis)
    {
        $this->street_bis = $street_bis;

        return $this;
    }

    /**
     * Get the value of zip
     */ 
    public function getZip()
    {
        return $this->zip;
    }

    /**
     * Set the value of zip
     *
     * @return  self
     */ 
    public function setZip($zip)
    {
        $this->zip = $zip;

        return $this;
    }

    /**
     * Get the value of city
     */ 
    public function getCity()
    {
        return $this->city;
    }

    /**
     * Set the value of city
     *
     * @return  self
     */ 
    public function setCity($city)
    {
        $this->city = $city;

        return $this;
    }

    /**
     * Get the value of county
     */ 
    public function getCountry()
    {
        return $this->county;
    }

    /**
     * Set the value of county
     *
     * @return  self
     */ 
    public function setCountry($county)
    {
        $this->county = $county;

        return $this;
    }
}

?>