<?php

namespace App\Repository;

use PDO;
use PDOException;
use App\Entity\Field;
use App\Repository\ManagerRepository;

class FieldRepository extends ManagerRepository {
    public function getAllField() {
        $sql = "SELECT * FROM field";
        $objects = [];

        try {
            // Connexion à la BDD
            $co = $this->dbConnexion();

            // Préparation et exécution de la requete SQL
            $stmt = $co->prepare($sql);
            $stmt->execute();

            // Récupéreation des donneés
            while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
                $objects[] = new Field($row);
            }
            $this->dbDeconnexion($co);
        } catch (PDOException $e) {
            throw $e->getMessage();
        }
        return $objects;
    }

    public function getOneField($idField) {
        $sql = "SELECT * FROM field WHERE id_field=:id";

        try {
            
            // Connexion à la BDD
            $co = $this->dbConnexion();
            // Préparation et exécution de la requete SQL
            $stmt = $co->prepare($sql);
            $stmt->bindParam(":id",$idField);
            $stmt->execute();

            $data = $stmt->fetch(PDO::FETCH_ASSOC);
            $this->dbDeconnexion($co);  
        } catch (PDOexception $e) {
            throw $e->getMessage();
        }
        return new Field($data);
    }

    public function addField(Field $field)
    {
        $sql = "INSERT INTO field (name,team,lat,lng,city,country) VALUES (:name,:team,:lat,:lng,:city,:country)";
        $name = $field->getName();
        $team = $field->getTeam();
        $lat = $field->getLat();
        $lng = $field->getLng();
        $city = $field->getCity();
        $country = $field->getCountry();

        try {
            $co = $this->dbConnexion();
            
            $stmt = $co->prepare($sql);
            $stmt->bindParam(":name", $name);
            $stmt->bindParam(":team", $team);
            $stmt->bindParam(":lat", $lat);
            $stmt->bindParam(":lng", $lng);
            $stmt->bindParam(":city", $city);
            $stmt->bindParam(":country", $country);
            $stmt->execute();

            $this->dbDeconnexion($co);  
            return true;
        } catch (PDOException $e) {
            throw $e;
        }
    }
    
}
