<?php

namespace App\Repository;

use PDO;
use PDOException;

/**
 * Manager Repository
 * 
 * Le manager repository nous permettra de gérer la connexion et la déconnexion à nos bases de données.
 */
class ManagerRepository
{
    private $serverName = "localhost";
    private $dbName = "baseball_field";
    private $port = "3306";
    private $userName = "root";
    private $password = "";

    /**
     * Database Connection
     * 
     * Permet la connexion à une base de données.
     */
    public function dbConnexion()
    {
        try {
            $co = new PDO(
                "mysql:host={$this->serverName};dbname={$this->dbName};port={$this->port};charset=utf8",
                $this->userName,
                $this->password
            );
    
            $co->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        } catch (PDOException $e) {
            echo $e->getMessage();
        }
        

        return $co;
    }

    /**
     * Database Déconnection
     * 
     * Permet de se déconnecter d'une base de données.
     */
    public function dbDeconnexion($co)
    {
        $co = null;
    }
}