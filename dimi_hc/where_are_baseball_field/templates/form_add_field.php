<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="public/build/app.css">
    <link rel="stylesheet" href="https://unpkg.com/leaflet@1.7.1/dist/leaflet.css" integrity="sha512-xodZBNTC5n17Xt2atTPuE1HxjVMSvLVW9ocqUKLsCC5CXdbqCmblAshOMAS6/keqq/sMZMZ19scR4PsZChSR7A==" crossorigin="" />
    <link rel="stylesheet" href="https://unpkg.com/leaflet.markercluster@1.4.1/dist/MarkerCluster.Default.css">
<link rel="stylesheet" href="https://unpkg.com/leaflet.markercluster@1.4.1/dist/MarkerCluster.css">
<link rel="preconnect" href="https://fonts.googleapis.com">
<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
<link href="https://fonts.googleapis.com/css2?family=Roboto&display=swap" rel="stylesheet">
    <title>Where are baselball field - Create New</title>
</head>

<body>
    <?php include 'menu.php' ?>

    <h1>Ajouter un terrain de baseball</h1>
    <?php if (isset($alert)){ ?>
        <p class="alert-success"><?=$alert ?></p>
    <?php } ?>
    <form action="?page=field&action=addField" method="post">
        <div>
            <input type="text" name="name" placeholder="Nom">
        </div>
        <div>
            <input type="text" name="team" placeholder="Équipe">
        </div>
        <div>
            <input type="number" name="lat" placeholder="Latitude">
        </div>
        <div>
            <input type="number" name="lng" placeholder="Longitude">
        </div>
        <div>
            <input type="text" name="city" placeholder="ville">
        </div>
        <div>
            <input type="text" name="country" placeholder="Pays">
        </div>
        <input type="submit" class="btn btn-secondary btn-large" value="ADD A LA BDD">
    </form>

    <div id="map" style="width: 100%; height:30vh"></div>

    <script src="https://unpkg.com/leaflet@1.7.1/dist/leaflet.js" integrity="sha512-XQoYMqMTK8LvdxXYG3nZ448hOEQiglfqkJs1NOQV44cWnUrBc8PkAOcXy20w0vlaXaVUearIOBhiXZ5V3ynxwA==" crossorigin=""></script>

    <script>
        // Code de fonctionnement de la map
        let map = L.map('map').setView([49.10383983947345, -1.2407788442681587],130)

        L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
            attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
        }).addTo(map);

        map.on('click', function(e) {
            let marker = new L.marker(e.latlng).addTo(map);
            console.log(e.latlng)
        })
    </script>           
</body>

</html>