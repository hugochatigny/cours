<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="public/build/app.css">
    <link rel="stylesheet" href="public/build/home.css">
    <link rel="stylesheet" href="https://unpkg.com/leaflet.markercluster@1.4.1/dist/MarkerCluster.css">
    <link rel="preconnect" href="https://fonts.googleapis.com">
<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
<link href="https://fonts.googleapis.com/css2?family=Roboto&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="https://unpkg.com/leaflet@1.7.1/dist/leaflet.css" integrity="sha512-xodZBNTC5n17Xt2atTPuE1HxjVMSvLVW9ocqUKLsCC5CXdbqCmblAshOMAS6/keqq/sMZMZ19scR4PsZChSR7A==" crossorigin="" />
    <title>Where are Baseball Field</title>
    <link rel="stylesheet" href="https://unpkg.com/leaflet.markercluster@1.4.1/dist/MarkerCluster.Default.css">
<link rel="stylesheet" href="https://unpkg.com/leaflet.markercluster@1.4.1/dist/MarkerCluster.css">


</head>

<body>
    <?php include 'menu.php'?>
    <h1>Where are Baseball Field</h1>

    <div class="container">
        <section class="field-cards">
            <?php foreach ($fields as $field) { ?>
                <div class="card mt-32 p-16" id="card-<?=$field->getidField()?>">
                    <h2><?= $field->getName(); ?></h2>
                    <h3><?= $field->getTeam(); ?></h3>
                    <p><?= $field->getDescription(); ?></p>
                    <div>
                        <span>Lat : <?= $field->getLat(); ?></span>
                        <span>lng : <?= $field->getLng(); ?></span>
                    </div>
                    <br>
                    <div>
                        <span><?= $field->getCity(); ?></span>
                        <span><?= $field->getCountry(); ?></span>
                    </div>
                    <div>
                        <a href="?page=field&id=<?=$field->getIdField()?>" class="btn btn-primary btn-medium ">voir plus</a>
                    </div>
                </div>
            <?php } ?>
        </section>

        <section class="map" id="map"></section>
    </div>


    <script src="https://unpkg.com/leaflet@1.7.1/dist/leaflet.js" integrity="sha512-XQoYMqMTK8LvdxXYG3nZ448hOEQiglfqkJs1NOQV44cWnUrBc8PkAOcXy20w0vlaXaVUearIOBhiXZ5V3ynxwA==" crossorigin=""></script>

    <script src="https://unpkg.com/leaflet.markercluster@1.4.1/dist/leaflet.markercluster.js"></script>


    <script>
        // Code de fonctionnement de la map
        let map = L.map('map')
            //.setView([51.505, -0.09], 13);
            .fitBounds([
                <?php foreach ($fields as $field) { ?>
                    [<?= $field->getLat() ?>, <?= $field->getLng() ?>],
                <?php } ?>

            ])

        L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
            attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
        }).addTo(map);

        L.popup({
            className:"popup"
        })

       

        let molochIcon = L.icon({
            iconUrl: 'public/img/momo_icon.png',
            iconSize: [75, 75],
            iconAnchor: [42, 84],
            popupAnchor: [-3, -76],
        });
       

        


        // Cluster de marker

        let markersCluster = new L.markerClusterGroup({

            showCoverageOnHover: false,
        });


        let latLng = ""
        let marker = ""
        let card = ""

        <?php foreach ($fields as $field) { ?>
            latLng = new L.LatLng(<?= $field->getLat() ?>, <?= $field->getLng() ?>);
            marker = new L.Marker(latLng, {title: "<?= $field->getName() ?>", icon : molochIcon});
            card = document.getElementById("card-<?=$field->getIdfield()?>")
            marker
                .bindPopup(card.innerHTML,{ className:"popup"})
                .addEventListener("click",() => {
                    console.log("<?=$field->getName()?>")
                    window.location.href='?page=field&id=<?= $field->getIdField() ?>'   
                })
                .on("mouseover",function(e){
                    document.getElementById("card-<?=$field->getIdfield()?>").classList.add("active")
                    this.openPopup()
                })
                .on("mouseout",function(e) {
                    document.getElementById("card-<?=$field->getIdfield()?>").classList.remove("active")
                    // this.closePopup()
                })

            markersCluster.addLayer(marker);
        <?php } ?>
        map.addLayer(markersCluster)
    </script>



</body>

</html>